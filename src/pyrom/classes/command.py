import abc
import logging
from collections import OrderedDict
from pyrom.constants import merc

logger = logging.getLogger(__name__)


PRIMARY_COMMANDS = [
    "north",
    "east",
    "south",
    "west",
    "up",
    "down",
    "at",
    "buy",
    "cast",
    "follow",
    "goto",
    "group",
    "hit",
    "inventory",
    "kill",
    "look",
    "who",
    "autolist",
]

# Setting default commands in the command table dict to
# make sure they resolve before others.
CMD_TABLE_DICT = OrderedDict()
for command in PRIMARY_COMMANDS:
    CMD_TABLE_DICT[command] = None

class Command:
    """Base class for commands."""
    __metaclass__ = abc.ABCMeta

    cmd_table = CMD_TABLE_DICT
    _required_attrs = ['name', 'position', 'do_fun']

    name = None
    position = None
    level = 0
    log = merc.LOG_NORMAL
    show = 1
    default_arg = None

    def __init_subclass__(cls, *args, **kwargs):
        super().__init_subclass__(*args, **kwargs)
        missing_attrs = []
        for attr in cls._required_attrs:
            if getattr(cls, attr, None) is None:
                missing_attrs.append(attr)
        if missing_attrs:
            raise NotImplementedError(f"The following required attributes are not implemented: {','.join(missing_attrs)}")

        cls.register_command(cls)

    @classmethod
    def do_fun(cls, ch, arguments):
        logger.error("%s command called by %s with arguments: %s", cls.name, ch.name, arguments)
        cls.fun(ch, arguments)

    @classmethod
    def register_command(cls, cmd_instance):
        cls.cmd_table[cmd_instance.name] = cls
