"""
/***************************************************************************
 *  Original Diku Mud copyright (C) 1990, 1991 by Sebastian Hammer,        *
 *  Michael Seifert, Hans Henrik St{rfeldt, Tom Madsen, and Katja Nyboe.   *
 *                                                                         *
 *  Merc Diku Mud improvments copyright (C) 1992, 1993 by Michael          *
 *  Chastain, Michael Quan, and Mitchell Tse.                              *
 *                                                                         *
 *  In order to use any part of this Merc Diku Mud, you must comply with   *
 *  both the original Diku license in 'license.doc' as well the Merc       *
 *  license in 'license.txt'.  In particular, you may not remove either of *
 *  these copyright notices.                                               *
 *                                                                         *
 *  Much time and thought has gone into this software and you are          *
 *  benefitting.  We hope that you share your changes too.  What goes      *
 *  around, comes around.                                                  *
 ***************************************************************************/

/***************************************************************************
*	ROM 2.4 is copyright 1993-1998 Russ Taylor			                   *
*	ROM has been brought to you by the ROM consortium		               *
*	    Russ Taylor (rtaylor@hypercube.org)				                   *
*	    Gabrielle Taylor (gtaylor@hypercube.org)			               *
*	    Brian Moore (zump@rom.org)					                       *
*	By using this code, you have agreed to follow the terms of the	       *
*	ROM license, in the file Rom24/doc/rom.license			               *
***************************************************************************/
/************
 * Ported to Python by Davion of MudBytes.net
 * Using Miniboa https://code.google.com/p/miniboa/
 * Now using Python 3 version https://code.google.com/p/miniboa-py3/
 ************/
"""
import logging
import os
import sys

"""Configs for the library."""

import logging
import logging.config
import os

LIBRARY_NAME = os.path.basename(os.path.dirname(__file__))

# Formats for logging
LOGGING_DATE_FORMAT = "%m/%d/%Y %I:%M:%S %p"
NORMAL_LOGGING_FORMAT = "%(asctime)s %(levelname)-8s [%(name)s] %(message)s"
VERBOSE_LOGGING_FORMAT = "%(asctime)s %(levelname)-8s [%(name)s.%(funcName)s:%(lineno)d] %(message)s"

# Configurable logging settings
LOGGING_LEVEL = os.environ.get(f"{LIBRARY_NAME.upper()}_LOGGING_LEVEL", logging.DEBUG)
LOGGING_FORMATTER = os.environ.get(f"{LIBRARY_NAME.upper()}_LOGGING_VERBOSITY", "normal")

# General logging config here.
LOGGING_CONFIG = {
    "version": 1,
    "disable_existing_loggers": False,
    "formatters": {
        "verbose": {
            "datefmt": LOGGING_DATE_FORMAT,
            "format": VERBOSE_LOGGING_FORMAT,
        },
        "normal": {
            "datefmt": LOGGING_DATE_FORMAT,
            "format": NORMAL_LOGGING_FORMAT,
        },
    },
    "handlers": {
        "console": {
            "level": LOGGING_LEVEL,
            "class": "logging.StreamHandler",
            "formatter": LOGGING_FORMATTER,
        },
        "null": {
            "level": LOGGING_LEVEL,
            "class": "logging.NullHandler",
            "formatter": LOGGING_FORMATTER,
        },
    },
    "loggers": {
        "": {
            "handlers": ["console"],
            "level": LOGGING_LEVEL,
        },
    },
}

# Configure the logging, and then log a message after import of the library.
logging.config.dictConfig(LOGGING_CONFIG)
LOGGER = logging.getLogger(LIBRARY_NAME)
LOGGER.info(f"Loaded {LIBRARY_NAME} library.")


sys.path.append(os.getcwd())
logger = logging.getLogger(__name__)

import time

from comm import close_socket, game_loop, init_descriptor
from hotfix import init_monitoring
from miniboa import TelnetServer
from pyrom.settings import PORT

startup_time = time.time()


def Pyom():
    sys.path.append(os.getcwd())
    logger.info("Logging system initialized.")
    server = TelnetServer(port=PORT)
    server.on_connect = init_descriptor
    server.on_disconnect = close_socket

    init_monitoring()
    logger.info("Entering Game Loop")
    game_loop(server)
    logger.critical("System halted.")


if __name__ == "__main__":
    Pyom()
