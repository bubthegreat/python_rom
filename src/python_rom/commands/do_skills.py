import collections
import logging

import const
import game_utils
import instance
import interp
from pyrom.constants import merc

logger = logging.getLogger(__name__)

def do_skills(ch, argument):
    fAll = False
    min_lev = 1
    max_lev = merc.LEVEL_HERO
    level = 0
    skill = None
    if ch.is_npc():
        return
    argument = argument.lower()
    if argument:
        fAll = True

        if not "all".startswith(argument):
            argument, arg = game_utils.read_word(argument)
            if not arg.isdigit():
                ch.send("Arguments must be numerical or all.\n")
                return

            max_lev = int(arg)

            if max_lev < 1 or max_lev > merc.LEVEL_HERO:
                ch.send("Levels must be between 1 and %d.\n" % merc.LEVEL_HERO)
                return

            if argument:
                argument, arg = game_utils.read_word(argument)
                if not arg.isdigit():
                    ch.send("Arguments must be numerical or all.\n")
                    return

                min_lev = max_lev
                max_lev = int(arg)

                if max_lev < 1 or max_lev > merc.LEVEL_HERO:
                    ch.send("Levels must be between 1 and %d.\n" % merc.LEVEL_HERO)
                    return

                if min_lev > max_lev:
                    ch.send("That would be silly.\n")
                    return


    available_skills = collections.defaultdict(list)

    for sn, skill in const.skill_table.items():
        if sn not in ch.learned:
            continue
        level = skill.skill_level.get(ch.guild.name)
        if level is None or level > max_lev:
            logger.debug("Skill %s is not available for character %s", sn, ch.name)
            continue
        if ch.level < level:
            learned_pct = " n/a"
        else:
            learned_pct = (ch.learned.get(sn, 1))

        available_skills[level].append((sn, learned_pct))

    if available_skills:
        for skill_level, level_skills in sorted(available_skills.items()):
            has_level = False
            col_count = 0
            for skill_name, learned_pct in sorted(level_skills):
                if col_count == 2:
                    line_buffer += "\n          "
                    col_count = 0
                if not has_level:
                    line_buffer = f"\nLevel {skill_level:2d}: "
                    has_level = True
                has_pct = "%" if isinstance(learned_pct, int) else ""
                actual_pct = f"{learned_pct:3d}" if isinstance(learned_pct, int) else f"{learned_pct:3s}"
                line_buffer += f"{skill_name:18s} {actual_pct}{has_pct}         "
                col_count += 1
            ch.send(line_buffer)
    else:
        ch.send("No skills found.\n")

    ch.send("\n")


interp.register_command(interp.cmd_type("skills", do_skills, merc.POS_DEAD, 0, merc.LOG_NORMAL, 1))
