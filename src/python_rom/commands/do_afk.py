from pyrom.classes.command import Command
from pyrom.constants import merc

class DoAFK(Command):

    name = 'afk'
    position = merc.POS_SLEEPING

    @classmethod
    def fun(cls, ch, arguments):
        if ch.comm.is_set(merc.COMM_AFK):
            ch.send("AFK mode removed. Type 'replay' to see tells.\n")
            ch.comm.rem_bit(merc.COMM_AFK)
        else:
            ch.send("You are now in AFK mode.\n")
            ch.comm.set_bit(merc.COMM_AFK)
