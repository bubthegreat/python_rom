import logging

import hotfix
import instance
import interp
from pyrom.constants import merc

logger = logging.getLogger(__name__)

def do_reload(ch, argument):
    logger.info("Files being reloaded by %s", ch.name)
    result = hotfix.reload_files()
    ch.send(result)


interp.register_command(interp.cmd_type("reload", do_reload, merc.POS_DEAD, merc.ML, merc.LOG_NORMAL, 1))
