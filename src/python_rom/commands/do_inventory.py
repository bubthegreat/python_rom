import logging

logger = logging.getLogger(__name__)

import handler_ch
import instance
import interp
from pyrom.constants import merc


def do_inventory(ch, argument):
    ch.send("You are carrying:\n")
    handler_ch.show_list_to_char(ch.inventory, ch, True, True)
    return


interp.register_command(interp.cmd_type("inventory", do_inventory, merc.POS_DEAD, 0, merc.LOG_NORMAL, 1))
