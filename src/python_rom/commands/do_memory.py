import logging

logger = logging.getLogger(__name__)

import interp
from pyrom.constants import merc

# TODO: Known broken.import instance


def do_memory(ch, argument):
    pass


interp.register_command(interp.cmd_type("memory", do_memory, merc.POS_DEAD, merc.IM, merc.LOG_NORMAL, 1))
