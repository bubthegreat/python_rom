from pyrom.classes.command import Command
from pyrom.constants import merc
import interp


class DoCommands(Command):

    name = 'commands'
    position = merc.POS_DEAD

    def fun(ch, argument):
        col = 0
        for key, cmd in interp.cmd_table.items():
            if cmd.level < merc.LEVEL_HERO and cmd.level <= ch.trust and cmd.show:
                ch.send("%-12s" % key)
                col += 1
                if col % 6 == 0:
                    ch.send("\n")
        for key, cmd in Command.cmd_table.items():
            if not cmd:
                continue
            if cmd.level < merc.LEVEL_HERO and cmd.level <= ch.trust and cmd.show:
                ch.send("%-12s" % key)
                col += 1
                if col % 6 == 0:
                    ch.send("\n")
        if col % 6 != 0:
            ch.send("\n")
        return


# import logging

# logger = logging.getLogger(__name__)

# import instance
# import interp
# from pyrom.constants import merc
# from pyrom.classes.command import Command


# def do_commands(ch, argument):
#     col = 0
#     for key, cmd in interp.cmd_table.items():
#         if cmd.level < merc.LEVEL_HERO and cmd.level <= ch.trust and cmd.show:
#             ch.send("%-12s" % key)
#             col += 1
#             if col % 6 == 0:
#                 ch.send("\n")
#     for key, cmd in Command.cmd_table.items():
#         if not cmd:
#             continue
#         if cmd.level < merc.LEVEL_HERO and cmd.level <= ch.trust and cmd.show:
#             ch.send("%-12s" % key)
#             col += 1
#             if col % 6 == 0:
#                 ch.send("\n")
#     if col % 6 != 0:
#         ch.send("\n")
#     return


# interp.register_command(interp.cmd_type("commands", do_commands, merc.POS_DEAD, 0, merc.LOG_NORMAL, 1))
