import logging

logger = logging.getLogger(__name__)

import instance
import interp
from pyrom.constants import merc


def do_bamfout(ch, argument):
    if not ch.is_npc():
        if not argument:
            ch.send("Your poofout is %s\n" % ch.bamfout)
            return
        if ch.name.lower() not in argument.lower():
            ch.send("You must include your name.\n")
            return
        ch.bamfout = argument
        ch.send("Your poofout is now %s\n" % ch.bamfout)
    return


interp.register_command(interp.cmd_type("poofout", do_bamfout, merc.POS_DEAD, merc.L8, merc.LOG_NORMAL, 1))
