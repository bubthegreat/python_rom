import logging

logger = logging.getLogger(__name__)

import interp
from pyrom.constants import merc

# TODO: Known brokenimport instance


def do_dump(ch, argument):
    pass


interp.register_command(interp.cmd_type("dump", do_dump, merc.POS_DEAD, merc.ML, merc.LOG_ALWAYS, 0))
