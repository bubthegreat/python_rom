import logging

logger = logging.getLogger(__name__)

import handler_ch
import instance
import interp
from pyrom.constants import merc


def do_south(ch, argument):
    handler_ch.move_char(ch, merc.DIR_SOUTH, False)
    return


interp.register_command(interp.cmd_type("south", do_south, merc.POS_STANDING, 0, merc.LOG_NEVER, 0))
