import logging

logger = logging.getLogger(__name__)

import instance
import interp
from pyrom.constants import merc


def do_bamfin(ch, argument):
    if not ch.is_npc():
        if not argument:
            ch.send("Your poofin is %s\n" % ch.bamfin)
            return
        if ch.name.lower() not in argument.lower():
            ch.send("You must include your name in your poofin.\n")
            return
        ch.bamfin = argument
        ch.send("Your poofin is now %s\n" % ch.bamfin)
    return


interp.register_command(interp.cmd_type("poofin", do_bamfin, merc.POS_DEAD, merc.L8, merc.LOG_NORMAL, 1))
