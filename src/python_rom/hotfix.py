import importlib
import logging
import os
import traceback

logger = logging.getLogger(__name__)

# dictionary of files to track. will be key'd by file name and the value will be modified unix timestamp
tracked_files = {}
modified_files = {}


def init_file(path, modules, silent=False):
    # called by init_monitoring to begin tracking a file.
    modules = [importlib.import_module(m) for m in modules]
    tracked_files[path] = [os.path.getmtime(path), modules]
    if not silent:
        logger.info("    Tracking %s", path)
    else:
        logger.debug("    Tracking %s", path)


def init_directory(path, silent=False):
    dir = os.listdir(path)
    files = [f for f in dir if not f.startswith("__")]

    logger.info("Tracking %d files in %s", len(files), path)
    for file in files:
        full_path = os.path.join(path, file)
        module = full_path.split(".")[0].replace(os.sep, ".")
        init_file(full_path, [module], silent)


def init_monitoring():
    # Called in main function to begin tracking files.
    logger.info("Monitoring system initializing...")
    init_file("handler_ch.py", ["handler_ch"])
    init_file("handler_item.py", ["handler_item"])
    init_file("handler_room.py", ["handler_room"])
    init_file("handler_log.py", ["handler_log"])
    init_file("shop_utils.py", ["shop_utils"])
    init_file("game_utils.py", ["game_utils"])
    init_file("pyprogs.py", ["pyprogs"])
    init_file("affects.py", ["affects"])
    init_file("effects.py", ["effects"])
    init_file("fight.py", ["fight"])
    init_directory(os.path.join("commands"))
    init_directory(os.path.join("spells"))
    logger.info("done. (Monitoring system)")


def poll_files():
    # Called in game_loop of program to check if files have been modified.
    for fp, pair in tracked_files.items():
        mod, modules = pair
        if mod != os.path.getmtime(fp):
            # File has been modified.
            logger.warn("%s has been modified", fp)
            tracked_files[fp][0] = os.path.getmtime(fp)
            modified_files[fp] = [os.path.getmtime(fp), modules]


def reload_files():
    errored_files = []
    default_result = "Files reloaded."
    for fp, pair in modified_files.copy().items():
        mod, modules = pair
        logger.warn("Reloading %s", fp)
        for m in modules:
            try:
                importlib.reload(m)
            except:
                errored_files.append(fp)
                logger.exception("Failed to reload %s", fp)

        del modified_files[fp]
    if errored_files:
        result = f"Failed to reload files: {' '.join(errored_files)}"
    else:
        result = default_result
    return result
