# Python ROM Port

This is a take-over of the original work for the python-rom port.  The code was partially complete, but now for the most part runs - the code is currently in various states of implementation, but will mostly run.  Current issues are mostly centered around state management not being consistent, and property/function references not being called consistently.

The current plan for continuing to maintain this code is to port it into two libraries - one library will be the python port, but will start with the more functional commands, and the second library will be the current implementation.  I'll slowly transition the current implementation into the "true" python port that's got package management strategies in place and has a more pythonic directory structure and import structure.  Objects will be used to carry/contain/manipulate state, and functional programming will be utilized for most of the rest of the application to make testability easier and more streamlined.

I plan on reworking the class objects to inherit more consistently for base classes, and then abstract from there, so that classes and races can be dropped in more readily to expand on the existing skillsets, classes, and races.

I plan on moving help into the commands themselves, and making a command class that contains some base requirements, so any command will have it's own help built into the help, rather than a separately maintained area file.

I also plan on ripping out miniboa in favor of the builtin library for asyncio and terminal libraries that are part of the standard library.

After all of that is implemented and it's transitioned, I'll be moving it to a simpler file strategy - most likely a sqlite implementation that can be exchanged for a real database implementation so that the state can be handled separately from the filesystem and upgrades/updates to it.

After all of *that* is implemented I'll likely be adding a kubernetes deployment config and implementing CI/CD so that code changes that are accepted are automatically deployed to the production instance.

This is going to be a lot of work - but much less than rewriting it from scratch. Help is most definitely wanted.

## Current Status
Running locally, but pretty buggy.  Got fights working again, most commands work again, general movement and exploration works, levels can be gained, skills can be practiced, trains can happen - but the PK system is still...undocumented.

Currently under heavy updates to get things running again.  To run the server locally:

1. `cd src/python_rom`
2. `python pyom.py`

There are two "default" characters right now used for testing:
user: bub
pass: asdfasdf

user: hess
pass: asdfasdf

The areas have also been slimmed down significantly for testing since it takes a bit to re-load everything in the json files again.

# Original info:
Original repo was at https://bitbucket.org/mudbytes/pyom/src/master/
github repo I started with was at https://github.com/quixadhal/PyRom

