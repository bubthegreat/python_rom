"""Setup configuration and dependencies for python_rom."""

from setuptools import find_packages
from setuptools import setup


REQUIREMENTS = [requirement for requirement in open("requirements.txt").readlines()]

COMMANDS = [
    "example_command=python_rom.example:main",
]

setup(
    name="python_rom",
    version="0.0.0.alpha0",
    author="Micheal Taylor",
    author_email="bubthegreat@gmail.com",
    url="",
    include_package_data=True,
    description="Python port of the ROM MUD",
    packages=find_packages('src'),
    package_dir={
        '': 'src',
    },
    python_requires=">=3.6.6",
    entry_points={"console_scripts": COMMANDS},
    install_requires=REQUIREMENTS,
)
